all:
	@go build .

install: all
	@rm -f /usr/local/bin/archttpd
	@cp archttpd /usr/local/bin

xdg:
	@xdg-mime install desktop/x-http+zip.xml
	@cp desktop/archttpd.desktop ~/.local/share/applications/

clean:
	@rm -f archttpd