# archttpd

[中文教程在此](https://gitlab.com/buckle2000/archttpd/wikis/home)

Yet another static HTTP server based on [PhysicsFS](http://icculus.org/physfs/)

It can serve contents in zip file directly.

## Synposis

`archttpd [-p <PORT>] <FILE OR DIRECTORY>`

## Try it out!

`go run *.go introduction/`

## Build

`go build -o archttpd *.go`

## Functions

[X] serve dir
[X] serve zip, lzma/7z, grp, pak, hog, mvl, wad
[X] auto append index.html/index.md to directory /xxx/
[X] auto append .html/.md
[X] auto open browser
[X] some protection
    [X] do not load anything external by default    (Content-Security-Policy)
    [X] prevent clickjack                           (X-Frame-Options: sameorigin)
    [X] prevent flash from loading external content (X-Permitted-Cross-Domain-Policies)
[X] render markdown

[ ] manual in ZH-CHT
[ ] use AppVeyor and [goreleaser](https://goreleaser.com/)
[ ] Unit test
[ ] compile static binary
[ ] serve msgarc

## Routing

When you visit a URL without extersion like `/abc`

- try serve `/abc`
- try serve `/abc.html`
- try render `/abc.md` and serve

But when you visit `/abc.md`, the server will serve the file directly (plain text).

### Adverseries

Don't open untrusted archive.
Opening untrusted archive is as safe as opening untrusted website.
javascript in an archive may send your IP address (via WebRTC), system information to local malware
