package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/integrii/flaggy"
	"github.com/spkg/zipfs"

	fileserver "./fs"
)

type securityHandler struct {
	inner      http.Handler
	disableCSP bool
}

func (h securityHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	header := res.Header()
	if !h.disableCSP {
		header.Set("Content-Security-Policy", "default-src 'self' 'unsafe-inline';") // prevent outside attack
	}
	header.Set("X-Frame-Options", "sameorigin")             // prevent clickjack
	header.Set("X-Permitted-Cross-Domain-Policies", "none") // fuck Adobe
	header.Set("X-Powered-By", "archttpd")                  // :)
	h.inner.ServeHTTP(res, req)
}

const helpExample = `
  Examples:
    %[1]v .            # serve current directory
    %[1]v -p 1234 .    # serve current directory on port 1234
    %[1]v archive.zip  # serve a ZIP archive`

func main() {
	// parser := argparse.NewParser("", "HTTP server for archive reading")
	var path string
	port := 0
	flagNoOpenBrowser := false
	flagDisableCSP := false
	flagDisableMarkdown := false

	flaggy.SetName(os.Args[0])
	flaggy.SetVersion("3.1.0")
	flaggy.SetDescription("HTTP server for archive reading")
	flaggy.AddPositionalValue(&path, "FILE", 1, true, "archive or directory to serve")
	flaggy.Int(&port, "p", "port", "listening port")
	flaggy.Bool(&flagNoOpenBrowser, "x", "no-open", "do not open page in browser")
	flaggy.Bool(&flagDisableCSP, "", "no-csp", "disable CSP (very dangerous!)")
	flaggy.Bool(&flagDisableMarkdown, "", "md", "render markdown")
	flaggy.DefaultParser.AdditionalHelpAppend = fmt.Sprintf(helpExample, os.Args[0])
	flaggy.Parse()
	{
		if port == 0 {
			var err error
			port, err = getFreePort()
			if err != nil {
				fmt.Println("Error:", err)
				os.Exit(1)
			}
		}
	}

	// Try mount FS
	var fs http.FileSystem
	{
		// check if exist
		stat, err := os.Stat(path)
		switch {
		case err != nil:
			fmt.Println("Error:", err)
			os.Exit(1)
			break
		case stat.IsDir():
			fs = http.Dir(path)
			break
		default:
			fsp, err := zipfs.New(path)
			if err != nil {
				fmt.Println("Error:", err)
				os.Exit(1)
			}
			fs = fsp
		}
	}

	addr := fmt.Sprintf("localhost:%d", port)
	url := fmt.Sprintf("http://%v", addr)
	var handler http.Handler
	if !flagDisableMarkdown {
		handler = fileserver.FileServer(fs)
	} else {
		handler = http.FileServer(fs)
	}
	handler = securityHandler{handler, flagDisableCSP}

	fmt.Printf("Serving %v at %v\n", path, url)
	if !flagNoOpenBrowser {
		startBrowser(url)
		fmt.Printf("A browser window should open. If not, please visit %s\n", url)
	}
	panic(http.ListenAndServe(addr, handler))
}
